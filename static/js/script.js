/**
 * Created by jankrloz on 23/03/17.
 */

wow = new WOW(
    {
        duration: '5s',
        boxClass: 'wow',      // default
        animateClass: 'animated', // default
        offset: 50,          // default
        mobile: false,       // default
        live: true        // default
    }
);
wow.init();

$(document).on('click', '[data-toggle="lightbox"]', function (event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});

jQuery(document).ready(function () {
    jQuery('.skillbar').each(function () {
        jQuery(this).find('.skillbar-bar').animate({
            width: jQuery(this).attr('data-percent')
        }, 6000);
    });

    $('#btn-mas-empleos').on('click', function () {
        $('#row-mas-empleos').slideToggle();
    });

});

(function () {

    $('.slick-container').slick({
        lazyLoad: 'ondemand',
        slidesToShow: 5,
        slidesToScroll: 5,
        autoplay: true,
        autoplaySpeed: 1000,
        infinite: true,
        mobileFirst: true,
        variableWidth: true,
        centerMode: true
    });

    $("#back3").delay(800).fadeOut(1000, function () {
        $("#back2").fadeOut(1000, function () {
            $("#back1").fadeOut(1000);
            $("#back3").fadeIn(1000);
        });
    });
})(jQuery);
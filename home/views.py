from django.http import HttpResponse
from django.shortcuts import render


# Create your views here.

def home(request):
    import datetime as date
    from datetime import datetime as datetime
    from django.conf import settings
    birth_date = date.date(1992, 12, 10)
    age = int((datetime.now().date() - birth_date).days / 365.25)
    return render(request, 'home.html', {'age': age, 'DEBUG': settings.DEBUG})
